﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;

namespace HuffmanTree
{
    using HuffmanForestType = SortedDictionary<int, List<Node>>;
    class Node : IComparable<Node>
    {
        public Node RightChild;
        public int Weight;
        public byte Character;
        public Node LeftChild;

        private int NodeNumber;

        private static int NextNodeNumber = 0;

        public static Node CreateLeaf(byte character)
        {
            return new Node(1, character, null, null);
        }
        public static Node CreateInner(
            int weight,
            Node leftChild,
            Node rightChild)
        {
            return new Node(weight, leftChild.Character, leftChild, rightChild);
        }
        private Node(
            int weight,
            byte character,
            Node leftChild,
            Node rightChild)
        {
            this.Weight = weight;
            this.Character = character;
            this.LeftChild = leftChild;
            this.RightChild = rightChild;
            this.NodeNumber = NextNodeNumber;
            NextNodeNumber++;
        }

        /// <summary>
        /// Determines if the node has children
        /// </summary>
        /// <returns>True if the node has no children</returns>
        public bool IsLeaf()
        {
            if ((LeftChild == null) && (RightChild == null))
            {
                return true;
            }
            else return false;
        }

        public static int SumWeights(Node first, Node second)
        {
            return first.Weight + second.Weight;
        }

        /// <summary>
        /// Increments weight of this node
        /// </summary>
        /// <param name="rank"></param>
        /// <returns>current node (fluent interface)</returns>
        public Node IncrementWeight(int rank)
        {
            Weight += rank;
            return this;
        }

        /// <summary>
        /// Determines whether this node will be left or right in Huffman Tree
        /// from a given node
        /// </summary>
        /// <param name="another">the other node to compare</param>
        /// <returns>True if this node will be left from the given node
        /// </returns>
        public bool IsLeftFrom(Node another)
        {
            if (another.Weight != Weight)
            {
                // if weight is different, we can decide by weight
                return another.Weight > Weight;
            }
            else if (another.IsLeaf() != IsLeaf())
            {
                // if weight was the same,
                // current node is left from another
                // if this is leaf and the other is not
                return IsLeaf();
            }
            else if (IsLeaf() && another.IsLeaf()
                && (Character != another.Character))
            {
                // if both nodes are leaves with the same weight but different
                // character, the leaf with the lower character number will be
                // left from the other
                return (Character < another.Character);
            }
            else {
                // otherwise, determine node order by the number
                return (NodeNumber < another.NodeNumber);
            }
        }


        #region IComparable Members

        public int CompareTo(Node obj)
        {
            if (this == obj)
            {
                return 0;
            }
            else if (IsLeftFrom(obj))
            {
                return -1;
            }
            else
            {
                return 1;
            }

        }

        #endregion
    }


    class TreeBuilder
    {
        private static int SumNodesInForest(HuffmanForestType huffmanForest)
        {
            int sum = 0;
            foreach (KeyValuePair<int, List<Node>> item in huffmanForest)
            {
                sum += item.Value.Count;
            }
            return sum;
        }

        private static void AddNodeToForest(HuffmanForestType forest, Node node)
        {
            if (forest.ContainsKey(node.Weight))
            {
                forest[node.Weight].Add(node);
            }
            else
            {
                forest.Add(node.Weight, new List<Node>() { node });
            }
        }

        /// <summary>
        /// Builds a Huffman tree from nodes organized into Huffman forest
        /// structure. Warning: the passed huffmanForest object is altered
        /// during execution.
        /// </summary>
        /// <param name="huffmanForest"></param>
        /// <returns></returns>
        public static Node Build(HuffmanForestType huffmanForest)
        {
            Node odd = null;
            int countOfNodesToProcess = SumNodesInForest(huffmanForest);

            while (countOfNodesToProcess != 1)
            {
                //take nodes with lowest weight
                int weight = huffmanForest.Keys.ElementAt(0);
                List<Node> nodes = huffmanForest[weight];
                if (odd != null)
                {
                    nodes.Insert(0, odd);
                    odd = null;
                }
                for (int i = 0; i < nodes.Count - 1; i+=2)
                {
                    Node node = nodes[i];
                    Node nextNode = nodes[i+1];
                    Node[] pair = new Node[] { node, nextNode };
                    
                    //left node will be first, right node second
                    Array.Sort(pair);
                    Node newNode = Node.CreateInner(
                        node.Weight + nextNode.Weight, pair[0], pair[1]);

                    AddNodeToForest(huffmanForest, newNode);

                    countOfNodesToProcess--;
                }
                if (nodes.Count % 2 == 1)
                {
                    odd = nodes[nodes.Count - 1];
                }
                huffmanForest.Remove(weight);
            }
            return huffmanForest[huffmanForest.Keys.ElementAt(0)][0];
        }

        
    }

    class TreePrinter
	{
		/// <summary>
		/// Prints the tree to console.
		/// </summary>
        public static void Print(Node root)
        {
            Print(root, "");
        }

        private static void PrintLeaf(Node node) {
            if ((node.Character >= 32) && (node.Character <= 0x7E))
            {
                PrintLeafAsCharacter(node);
            }
            else
            {
                PrintLeafAsByteNumber(node);
            }
        }

        private static void PrintLeafAsCharacter(Node node)
        {
            Console.Write(" ['{0}':{1}]\n", (char)node.Character, node.Weight);
        }

        private static void PrintLeafAsByteNumber(Node node)
        {
            Console.Write(" [{0}:{1}]\n", node.Character, node.Weight);
        }

        private static void Print(Node node, string indentation)
        {

            if (node.IsLeaf())
            {
                PrintLeaf(node);
            }
            else
            {
                string childIndentation = IncreaseIndentation(indentation);
                PrintInnerNode(node);
                Print(node.RightChild, childIndentation + "|  ");
                PrintEdgeToRightChild(childIndentation);
                Print(node.LeftChild, childIndentation + "   ");
            }
        }

        private static void PrintInnerNode(Node node)
        {
            Console.Write("{0,4} -+- ", node.Weight);
        }

        private static void PrintEdgeToRightChild(string indentation)
        {
            Console.Write("{0}|\n", indentation);
            Console.Write("{0}`- ", indentation);
        }

        private static string IncreaseIndentation(string indentation) {
            return indentation + "      ";
        }
    }

    class NodeLoader
    {
        public class CannotReadNodeException : Exception { }
        
        private const int ReadBufferSize = 0x4000;
        private const int DifferentCharacters = 0x100;
        /// <summary>
        /// Opens file and checks if it can be read from, throws exception
        /// on any failure
        /// </summary>
        /// <param name="filename"></param>
        /// <returns>stream to the opened file</returns>
        private static FileStream OpenFile(string filename)
        {
            FileStream input = new FileStream(
                filename, FileMode.Open, FileAccess.Read);
            if (!(input.CanRead))
            {
                throw new CannotReadNodeException();
            }

            return input;
        }

        private static Node[] ReadNodesByCharacter(FileStream input) {
            Node[] nodesByCharacter = new Node[DifferentCharacters];
            byte[] readBuffer = new byte[ReadBufferSize];
            int bytesRead;
            do
            {
                bytesRead = input.Read(readBuffer, 0, ReadBufferSize);

                for (int j = 0; j < bytesRead; j++)
                {
                    byte character = readBuffer[j];
                    IncrementCharacterWeight(nodesByCharacter, character);
                }
            } while (bytesRead == ReadBufferSize);
            return nodesByCharacter;
        }
        private static void IncrementCharacterWeight(
            Node[] nodesByCharacter,
            byte character)
        {
            if (nodesByCharacter[character] == null)
            {
                nodesByCharacter[character] = Node.CreateLeaf(character);
            }
            else
            {
                nodesByCharacter[character].Weight++;
            }
        }

        private static HuffmanForestType GroupNodesByWeight(
            Node[] nodeByCharacter)
        {
            HuffmanForestType forest = new HuffmanForestType();
            for (int i = 0; i < DifferentCharacters; i++)
            {
                if (nodeByCharacter[i] != null)
                {
                    InsertNodeByWeight (forest, nodeByCharacter[i]);   
                }
            }

            return forest;
        }
        private static void InsertNodeByWeight(
            HuffmanForestType forest,
            Node node)
        {
            if (forest.ContainsKey(node.Weight))
            {
                forest[node.Weight].Add(node);
            }
            else forest.Add(node.Weight, new List<Node>() { node });
        }

        private static void SortNodesByPositionInTree(HuffmanForestType forest)
        {
            foreach (KeyValuePair<int, List<Node>> item in forest)
            {
                item.Value.Sort();
            }
        }
		/// <summary>
		/// Reads file and returns the tree (forest).
		/// </summary>
		/// <param name="filename"></param>
		/// <returns>HuffmanForestType desired forest</returns>
        public static HuffmanForestType LoadForestFromFile(string filename)
        {

            FileStream input = OpenFile(filename);

            Node[] nodesByCharacter = ReadNodesByCharacter(input);

            HuffmanForestType forest = GroupNodesByWeight(nodesByCharacter);

            SortNodesByPositionInTree(forest);

            return forest;
        }

    }

    class Program
    {
        //   static Stopwatch sw = new Stopwatch();

        static void Main(string[] args)
        {
            //     sw.Start();

            if (args.Length != 1)
            {
                Console.Write("Argument Error");
                Environment.Exit(0);
            }

            try
            {
                HuffmanForestType forest =
                    NodeLoader.LoadForestFromFile(args[0]);
                if (forest.Count != 0)
                {
                    TreePrinter.Print(TreeBuilder.Build(forest));
                    Console.Write("\n");
                }

                /*      sw.Stop();
                      string ExecutionTimeTaken = string.Format(
                          "Minutes :{0}\nSeconds :{1}\n Mili seconds :{2}",
                          sw.Elapsed.Minutes,
                          sw.Elapsed.Seconds,
                          sw.Elapsed.TotalMilliseconds);
                      Console.Write(ExecutionTimeTaken);
                      Console.ReadKey();

                      Console.ReadKey(); */
            }
            catch (Exception)
            {
                Console.Write("File Error");
                Environment.Exit(0);
            }
        }
    }
}
